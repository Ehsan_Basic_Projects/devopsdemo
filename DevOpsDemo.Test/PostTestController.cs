using DevOpsDemo.Controllers;
using DevOpsDemo.Models;
using DevOpsDemo.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Xunit;
namespace DevOpsDemo.Test
{
    public class PostTestController
    {
        /*
        private PostRepository repository;
        public PostTestController()
        {
            repository = new PostRepository();
        }
        [Fact]
        public void Test_Index_View_Result()
        {
            //Arrange
            var controller = new HomeController(this.repository);
            //Act
            var result = controller.Index();
            //Assert
            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public void Test_Index_Return_Result()
        {
            //Arrange
            var controller = new HomeController(this.repository);
            //Act
            var result = controller.Index();
            //Assert
            Assert.NotNull(result);
        }
        [Fact]
        public void Test_Index_GetPosts_MatchData()
        {
            //Arrange
            var controller = new HomeController(this.repository);
            //Act
            var result = controller.Index();
            //Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<List<PostViewModel>>(viewResult.ViewData.Model);
            Assert.Equal(3, model.Count);
            Assert.Equal(101, model[0].PostId);
            Assert.Equal("DevOps Demo Title 1", model[0].Title);
        }
        */

        [Fact]
        public void Test1()
        {   
            Assert.Equal("1", "1");
        }

        [Fact]
        public void Test2()
        {
            Assert.Equal("2", "2");
        }

        [Fact]
        public void Test3()
        {
            Assert.Equal("3", "3");
        }

        [Fact]
        public void Test4()
        {
            Assert.Equal("4", "4");
        }

        [Fact]
        public void Test5()
        {
            Assert.Equal("5", "5");
        }

        [Fact]
        public void Test6()
        {
            Assert.Equal("6", "6");
        }

        [Fact]
        public void Test7()
        {
            Assert.Equal("7", "7");
        }
    }
}
